// File: avc-examples-ui3/src/main/www/js/framework.js

let app = null;

// Global "pre_app" object with custom fields and custom methods.
//
const pre_app = {

	buildinfo: null,
	auth: null,
	auth_checked: false,
};

// This "getApp()" functions is for testability, since Selenium cannot read
// global variables such as "app", but can call global functions such as
// "getApp()"
//
function getApp() {

	return app;
}

async function async_getApp() {

	const promise = new Promise(function(resolve) {

		let timerId = setTimeout(function tick() {

			if (app !== null) {

				clearTimeout(timerId);

				resolve(app);

			} else {

				timerId = setTimeout(tick, 100);
			}

		}, 100);
	});

	return await promise;
}

// Fix a "XML Parsing Error: not well-formed" error in Firefox
//
$.ajaxSetup({
	// TODO is this still necessary?
	beforeSend: function(xhr) {
		if (xhr.overrideMimeType) {
			xhr.overrideMimeType('application/json');
		}
	}
});

// Same here: Add "Content-Type: application/json"
//
jQuery['post_object'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = null;
	}

	return $.ajax({
		url: url,
		type: 'POST',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		dataType: 'json',
		data: JSON.stringify(data),
		success: callback
	});
};

jQuery['post_plain_text'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = null;
	}

	return $.ajax({
		url: url,
		type: 'POST',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		dataType: 'text',
		data: JSON.stringify(data),
		success: callback
	});
};

jQuery['delete_plain_text'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	return $.ajax({
		url: url,
		type: 'DELETE',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		dataType: 'text',
		data: data,
		success: callback
	});
};

// Same here: Add "Content-Type: application/json"
//
jQuery['get_object'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	return $.ajax({
		url: url,
		type: 'GET',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		dataType: 'json',
		data: data,
		success: callback
	});
};

jQuery['get_object_or_null'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	$.get_object(url, data, callback).fail(function() {

		callback(null);
	});
}

jQuery['get_boolean'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	$.get_object(url, data, function(json) {

		callback(json !== null && json === true);

	}).fail(function() {

		callback(false);
	});
}

// Same here: Add "Content-Type: application/json"
//
jQuery['put_object'] = function(url, data, callback) {

	if (jQuery.isFunction(data)) {
		callback = data;
		data = undefined;
	}

	return $.ajax({
		url: url,
		type: 'PUT',
		contentType: 'application/json; charset=utf-8',
		xhrFields: { withCredentials: true },
		dataType: 'json',
		data: JSON.stringify(data),
		success: callback
	});
};

function on(condition, action, after) {

	let timerId = setTimeout(function tick() {

		if (condition()) {

			clearTimeout(timerId);

			if (action.name === 'load_app') {

				const load_app = action;

				if (app) { return; }

				app = load_app();

			} else {

				action();
			}

			if (after) {
				after();
			}

		} else {

			timerId = setTimeout(tick, 100);
		}

	}, 100);
}


// Load the buildinfo (version, etc.) from a static file
//
$.get_object(BASE_HREF + 'buildinfo.json', function(json) {
	pre_app.buildinfo = json;
});

// Load the authenticated object
//
$.get_object(CONFIG.USERS_API_BASE_URL + '/auth', function(json) {

	if (json) {

		pre_app.auth = json;
		pre_app.auth_checked = true;
	
	} else {

		pre_app.auth = null;
		pre_app.auth_checked = true;
	}

}).fail(function(xhr, status) {

	pre_app.auth = null;
	pre_app.auth_checked = true;
});
