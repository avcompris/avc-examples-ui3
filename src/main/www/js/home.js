// File: avc-examples-ui3/src/main/www/js/home.js

// 1. INITIALIZE THE CONTEXT

// Define: app // From framework.js // Will be populated with the Vue object
// Load: pre_app.buildinfo // From framework.js

// 2. LOAD THE VUE INSTANCE

on(function condition() {

	return pre_app.buildinfo !== null && pre_app.auth_checked;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			auth: null,
			username: null,
			password: null,
		},
		methods: {

			authenticate: function() {

				const username = this.username ? this.username.trim() : null;
				const password = this.password ? this.password.trim() : null;

				if (!username || !password) {
					$('#div-flash-authError').removeClass('hidden');
					return;
				}

				$('#div-flash-authError').addClass('hidden');

				$.post_object(CONFIG.USERS_API_BASE_URL + '/auth', {
					username: username,
					password: password,
				}, function() {

					$.get_object(CONFIG.USERS_API_BASE_URL + '/auth', function(json) {

						update_auth(json);
					});

					window.location.replace(BASE_HREF);

				}).fail(function(xhr, status) {

					app.password = null;

					if (xhr.status === 401) {
						$('#div-flash-authError').removeClass('hidden');
					} else {
						alert('Something went wrong: '
								+ '{context: authenticate()'
								+ ', status: ' + status
								+ ', xhr.status: ' + xhr.status
								+ '}');
					}
				});
			},
		},
		computed: {
			authenticated: function() {
				return this.auth !== null;
			},
		},
	});

}, function after() {

	// 3. LOAD STUFF DEPENDING ON THE VUE INSTANCE

	$.get_object(CONFIG.USERS_API_BASE_URL + '/auth', function(json) {

		update_auth(json === undefined ? null : json);

	}).fail(function(xhr, status) {

		update_auth(null);
	});
});

function update_auth(auth) {

	if (auth !== null) {

		app.auth = auth;

		$('body').removeClass('unauthenticated');

//		$.get_string(CONFIG.USERS_API_BASE_URL + '/me/preferredLang', function(preferredLang) {
//		
//			update_lang(preferredLang ? preferredLang : default_lang);
//
//		}).fail(function() {
//
//			update_lang(default_lang);
//		});

	} else {

		app.auth = null;

		$('body').addClass('unauthenticated');
		$('body').removeClass('authenticated');
	}
}

function hide_flash_authError() {

	$('#div-flash-authError').addClass('hidden');
}
