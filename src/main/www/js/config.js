// File: avc-examples-ui3/src/main/www/js/config.js

const CONFIG_MODE = 'dev';

const CONFIG =

CONFIG_MODE === 'dev' ? {
	USERS_API_BASE_URL: 'http://192.168.54.1:8080/api/v1',
} :
CONFIG_MODE === 'qa_soy8' ? {
	USERS_API_BASE_URL: 'https://examples3.avcompris.com/api/v1',
} :
CONFIG_MODE === 'jenkins-ks1' ? {
	USERS_API_BASE_URL: 'http://docker:18080/api/v1',
} :
CONFIG_MODE === 'jenkins-ks2e' ? {
	USERS_API_BASE_URL: 'http://docker:28080/api/v1',
} :
CONFIG_MODE === 'jenkins-ks11' ? {
	USERS_API_BASE_URL: 'http://docker:38080/api/v1',
} :

null;
