// File: avc-examples-ui3/src/main/www/js/sign_out.js

// Load the Vue instance
//
on(function condition() {

	return pre_app.buildinfo !== null;

}, function load_app() {

	return new Vue({
		el: '#div-body',
		data: {
			buildinfo: pre_app.buildinfo,
			authentication: true,
		},
	});
});


$.get_object(CONFIG.USERS_API_BASE_URL + '/logout', function(json) {

	// Don’t use window.location.replace(), since we want to keep the navigation history
	window.location.href = BASE_HREF + '?cause=sign_out';

}).fail(function() {

	// Don’t use window.location.replace(), since we want to keep the navigation history
	window.location.href = BASE_HREF + '?cause=sign_out';
});
