#/bin/bash

# File: avc-examples-ui3/docker/avcompris/examples-ui3/entrypoint.sh

set -e

preamble() {

	echo "--------------------------------------------------------------------------------"
	echo "image: avcompris/examples-ui3"
	echo "--------------------------------------------------------------------------------"
	echo "buildinfo:"
	cat /buildinfo | sort
	echo "--------------------------------------------------------------------------------"
	echo -n Date\ && date
}

preamble

#-------------------------------------------------------------------------------
#   1. VALIDATIONS
#-------------------------------------------------------------------------------

if [ -z "${CONFIG_MODE}" -a -z "${USERS_API_BASE_URL}" ]; then
	echo "Either CONFIG_MODE should be set, of the form: \"dev\", \"qa_soy8\", \"jenkins-ks1\", etc." >&2
	echo "  or USERS_API_BASE_URL should be set, of the form: \"http://docker:18080/api/v1\"" >&2
	echo "Exiting." >&2
	exit 1
fi

#-------------------------------------------------------------------------------
#   2. OPTS
#-------------------------------------------------------------------------------

echo "CONFIG_MODE:        ${CONFIG_MODE}"
echo "USERS_API_BASE_URL: ${USERS_API_BASE_URL}"

if [ -n "${CONFIG_MODE}" ]; then
	sed -i "s/^const CONFIG_MODE =.*/const CONFIG_MODE = '${CONFIG_MODE}';/" \
		/usr/share/nginx/html/js/config.js
fi
if [ -n "${USERS_API_BASE_URL}" ]; then
	USERS_API_BASE_URL=`echo "${USERS_API_BASE_URL}" | sed s/\\\//\\\\\\\\\\\//g`
	sed -i "s/USERS_API_BASE_URL: .*/USERS_API_BASE_URL: '${USERS_API_BASE_URL}',/" \
		/usr/share/nginx/html/js/config.js
fi

#-------------------------------------------------------------------------------
#   3. START NGINX
#-------------------------------------------------------------------------------

# start nginx
echo "Starting nginx..."
nginx -g "daemon off;"